//select a database
use hotel

//insertOne
db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities.",
	rooms_available: 10,
	isAvailable: false
})

//insertMany
db.rooms.insertMany([
	{
		name: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation.",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway.",
		rooms_available: 15,
		isAvailable: false
	}
])

//find room name = double
db.rooms.find({name: "double"})

//    "_id" : ObjectId("62877fe615353ab1d269c3ae") <<< double

// updateOne, name:queen update to rooms_available: 0
db.rooms.updateOne(
	{_id:ObjectId("62877fe615353ab1d269c3af")},
	{
		$set: {
			rooms_available:0
		}
	}
)

//another solution
db.rooms.updateOne(
	{name: "queen"},
	{
		$set: {
			rooms_available:0
		}
	}
)

//deleteMany all rooms with rooms_available:0
db.rooms.deleteMany({
	rooms_available:0
})